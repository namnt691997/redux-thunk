import React from "react";
import "./App.css";
import { useDispatch } from "react-redux";

import { asynsAction, synsAction } from "./actions/exampleActions";

function App() {
  const dispatch = useDispatch();
  return (
    <div className="App">
      <button onClick={() => dispatch(synsAction({}))}>sync action</button>
      <button onClick={() => dispatch(asynsAction({}))}>async action</button>
    </div>
  );
}

export default App;
