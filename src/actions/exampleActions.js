import combineAsyncActions, {
  startAsyncAction,
  successAsyncAction,
  failAsyncAction,
} from "./combineAsyncActions";
import {
  START_ASYNC_ACTION_HOME,
  SUCCESS_ASYNC_ACTION_HOME,
  FAIL_ASYNC_ACTION_HOME,
  SYNC_ACTION,
} from "./actionTypes";

export const asynsAction = (data) =>
  combineAsyncActions(
    startAsyncAction({ type: START_ASYNC_ACTION_HOME }),
    successAsyncAction({ type: SUCCESS_ASYNC_ACTION_HOME }),
    failAsyncAction({ type: FAIL_ASYNC_ACTION_HOME }),
    async (payload) => {
      const data = await fetch("https://jsonplaceholder.typicode.com/todos/1");
      const json = await data.json();
      return json;
    }
  );

export const synsAction = (data) => {
  return {
    type: SYNC_ACTION,
  };
};
