export const startAsyncAction = ({ type, payload = {} }) => (asyncPayload) => {
  return {
    type,
    payload: { ...payload, ...asyncPayload },
  };
};

export const successAsyncAction = ({ type, payload = {} }) => (
  asyncPayload
) => {
  return {
    type,
    payload: { ...payload, ...asyncPayload },
  };
};
export const failAsyncAction = ({ type, payload = {} }) => (asyncPayload) => {
  return {
    type,
    payload: { ...payload, ...asyncPayload },
  };
};

const combineAsyncActions = (
  startAsyncAction,
  successAsyncAction,
  failAsyncAction,
  process = () => {}
) => async (dispatch, getState) => {
  startAsyncAction && dispatch(startAsyncAction());
  try {
    const data = await process();
    successAsyncAction && dispatch(successAsyncAction(data));
  } catch (error) {
    failAsyncAction && dispatch(failAsyncAction());
  }
};
export default combineAsyncActions;
