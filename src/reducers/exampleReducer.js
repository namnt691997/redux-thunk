import {
  START_ASYNC_ACTION_HOME,
  SUCCESS_ASYNC_ACTION_HOME,
  FAIL_ASYNC_ACTION_HOME,
  SYNC_ACTION,
} from "../actions/actionTypes";

const initState = {};

const exampleReducer = (state = initState, action) => {
  switch (action.type) {
    case START_ASYNC_ACTION_HOME: {
      return { ...state, ...action.payload };
    }
    case SUCCESS_ASYNC_ACTION_HOME: {
      return { ...state, ...action.payload };
    }
    case FAIL_ASYNC_ACTION_HOME:
      return { ...state, ...action.payload };
    case SYNC_ACTION:
      return { ...state };
    default:
      return state;
  }
};

export default exampleReducer;
